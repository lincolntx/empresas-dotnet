# Desafio Pessoa Desenvolvedora .NET

## Passos para executar o projeto

- O projeto tem um arquivo docker-compose que permite executar o projeto sem precisar do 
dotnet 3.1 nem o postgres instaldos nativamente na máquina
- Caso opte por não utilizar o docker pasta executar via terminal o comando ```dotent run```
dentro da pasta src.

### Executando Migrations
 - Para executar as migrations é necessários serguir os seguintes passos: 
    - Primeiro passo subir o banco de dados da aplicação
    
    Dentro da pasta empresas-dotnet execute o comando a baixo que builda o banco e o app:
    
    ```docker-compose up -d``` 
    
    Ou o Comando abaixo pra subir somento o banco:
    
    ```docker-compose up postgres -d```
    - Próximo passo para executar as migrations é necessário definir a variável ConnectionString:
    
    **PowerShell 7**
   
    ```
    $Env:ConnectionString = 'Server=127.0.0.1;Port=5432;Database=fake-imdb;User Id=postgres;Password=teste@12;MaxPoolSize=5;Pooling=true;SearchPath=public;'
    ```
   
   **Bash/Zsh**
   
   ```
   $Env:ConnectionString = 'Server=127.0.0.1;Port=5432;Database=fake-imdb;User Id=postgres;Password=teste@12;MaxPoolSize=5;Pooling=true;SearchPath=public;'
   ```
   
   A partir das variáveis de ambiente definidas, os comandos devem ser executados dentro do contexto da camanda de src:
   
    ```
    cd empresas-dotnet\src
    ```
   Uma vez dentro da pasta src pasta executar os comandos
   
   **Adicionar uma migração**
   
   ```shell
   dotnet ef migrations add InitialCreate
   ```
   
   **Atualizar as migrações existentes**
   
   ```shell
   dotnet ef database update
   ```
   
   
   Após finalizado não se esqueça de executar  o comando para matar o container docker para evitar conflitos futuros:
   
   ```docker-compose down```
### Executando com Docker
- Caso opte por utilizar o docker basta executar o comando ```docker-compose up -d```
    -  A tag menos -d é opicional, mas recomendo o uso caso não queira deixar seu terminal travado na execução ;
    - Caso utilize a tag -d não se esuqeça de rodar ```docker-compose down``` quando queriser 
    finalizar a execução do container.
    - Caso não utilize a tag -d bast apertar ```Ctrl + C``` para finalizar a execução
 
 **Lembre-se que antes de executar a aplicação é preciso ter configurado as migrations**

### Executar testes

Para executar os teste basta rodar ```dotnet test```na pasta ``empresas-dotent``

aplicação feita por [Lincoln Samuel F Teixeira](https://github.com/LincolnTx) email: lincolnsf98@gmail.com.