﻿using System;
using System.Threading.Tasks;
using empresas_dotnet.Application.Helpers.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace empresas_dotnet.Api.Filters
{
    public class AuthorizeAttribute : TypeFilterAttribute
    {
        
        public AuthorizeAttribute() : base(typeof(AuthorizeActionFilter))
        {
        }
    }

    public class AuthorizeActionFilter : IAsyncActionFilter
    {
        private readonly IIdentityService _identityService;

        public AuthorizeActionFilter(IIdentityService identityService)
        {
            _identityService = identityService;
        }

        public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            string authHeader = context.HttpContext.Request.Headers["Authorization"];
            string token = !(String.IsNullOrEmpty(authHeader)) ? authHeader.Replace("Bearer ", "") : "";
            
            try
            {
                var response = _identityService.IsTokenValid(token);
                
                if (response)
                {
                    await next();
                }
                else
                {
                    context.Result = new UnauthorizedResult();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
    }
}