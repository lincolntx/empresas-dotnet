﻿using System;
using empresas_dotnet.Filters.ErrorsModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace empresas_dotnet.Filters
{
    public class GlobalExceptionFilterAttribute : Attribute, IExceptionFilter
    {
        public GlobalExceptionFilterAttribute() { }

        public void OnException(ExceptionContext context)
        {
            context.Result = new BadRequestObjectResult(
                new DefaultError(false, 
                    new ErrorsResponse[]
                    {
                        new ErrorsResponse("188",
                            "Não foi possível realizar a operação",
                            DateTime.Now)
                    }
                )
            );
        }
    }
}