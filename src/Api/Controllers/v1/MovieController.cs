﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using empresas_dotnet.Api.Filters;
using empresas_dotnet.Application.Contracts.v1.Requests;
using empresas_dotnet.Application.Contracts.v1.Responses;
using empresas_dotnet.Application.Helpers.Identity;
using empresas_dotnet.Domain.Exceptions;
using empresas_dotnet.Infrastructure.Data.Repositories.MovieRepository;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Net.Http.Headers;

namespace empresas_dotnet.Api.Controllers.v1
{
    [Authorize]
    public class MovieController : BaseController
    {
        private readonly IMediator _mediator;
        private readonly IMovieRepository _movieRepository;
        private readonly IIdentityService _identityService;
        
        public MovieController(INotificationHandler<ExceptionNotification> notifications, 
            IMediator mediator, IMovieRepository movieRepository, 
            IIdentityService identityService) : base(notifications)
        {
            _mediator = mediator;
            _movieRepository = movieRepository;
            _identityService = identityService;
        }
        
        [HttpGet("list")]
        public async Task<IActionResult> List()
        {
            var moviesList = await _movieRepository.ListMovies();
            var result = new List<MovieListResponse>();
            moviesList.ForEach(movie =>
            {
                result.Add(new MovieListResponse(movie.Id.ToString(), movie.Title, movie.Genre,
                    movie.Actors, movie.AverageGrade));
            });
            return Response(200, new {movies = result });
        }
        
        [HttpPost]
        public async Task<IActionResult> Insert([FromBody] InsertMovieRequest movieRequest)
        {
            var accessToken = Request.Headers[HeaderNames.Authorization];
            var token = !(String.IsNullOrEmpty(accessToken)) ? accessToken.ToString().Replace("Bearer ", "") : "";
            var claims = _identityService.GetTokenClaims(token).ToList();

            var isAdmin = claims.Find(claim => claim.Type == "isAdmin")?.Value;
            if (!String.Equals(isAdmin, "True")) return new UnauthorizedResult();
            
            var movie = await _mediator.Send(movieRequest);

            return Response(200, movie);
        }
        
        [HttpPost("rate")]
        public async Task<IActionResult> RateMovie([FromBody] RateMovieRequest movieRequest)
        {
            var accessToken = Request.Headers[HeaderNames.Authorization];
            var token = !(String.IsNullOrEmpty(accessToken)) ? accessToken.ToString().Replace("Bearer ", "") : "";
            var claims = _identityService.GetTokenClaims(token).ToList();
            
            var userId = claims.Find(claim => claim.Type == "id")?.Value;
            movieRequest.UserId = userId;
            
            var movie = await _mediator.Send(movieRequest);

            return Response(200, movie);
        }
    }
}