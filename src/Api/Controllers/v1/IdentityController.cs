﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using empresas_dotnet.Application.Contracts.v1.Requests;
using empresas_dotnet.Domain.Exceptions;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace empresas_dotnet.Api.Controllers.v1
{
    public class IdentityController : BaseController
    {
        private readonly IMediator _mediator;
        public IdentityController(INotificationHandler<ExceptionNotification> notifications, IMediator mediator) : base(notifications)
        {
            _mediator = mediator;
        }

        [HttpPost("register")]
        public async Task<IActionResult> RegisterAsync([FromBody] CreateUserRequest createUserRequest)
        {
            var createdUser = await _mediator.Send(createUserRequest);

            return Response(200, createdUser);
        }
        [HttpPost("register-admin")]
        public async Task<IActionResult> RegisterAsync([FromBody] CreateAdminUserRequest createUserRequestRequest)
        {
            var createdUser = await _mediator.Send(createUserRequestRequest);

            return Response(200, createdUser);
        }
        
        [HttpPost("login")]
        public async Task<IActionResult> LoginAsync([FromBody] LoginUserRequest loginUserRequest)
        {
            var loginResponse = await _mediator.Send(loginUserRequest);

            return Response(200, loginResponse);
        }
    }
}
