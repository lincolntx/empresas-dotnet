﻿using System.Collections.Generic;
using empresas_dotnet.Domain.Exceptions;
using empresas_dotnet.Filters;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace empresas_dotnet.Api.Controllers
{
    [Route("[controller]")]
    [ServiceFilter(typeof(GlobalExceptionFilterAttribute))]
    public class BaseController : Controller
    {
        private readonly ExceptionNotificationHandler _notifications;
        protected IEnumerable<ExceptionNotification> Notifications;

        public BaseController(INotificationHandler<ExceptionNotification> notifications)
        {
            _notifications = (ExceptionNotificationHandler)notifications;
            Notifications = _notifications.GetNotifications();
        }

        protected bool IsValidOperation()
        {
            return (!_notifications.HasNotifications());
        }
        
        protected new IActionResult Response(int statusCode, object result = null)
        {
            if (IsValidOperation())
            {   
                return StatusCode(statusCode, new
                {
                    success = true,
                    data = result
                });
            }
            _notifications.Dispose();
            
            return BadRequest(new
            {
                success = false,
                data = Notifications
            });
        }
    }
}