﻿using System;

namespace empresas_dotnet.Infrastructure.Environment
{
    public class JwtSettings
    {
        public string Secret { get; set; }
        
        public TimeSpan TokenLifetime { get; set; }
    }
}