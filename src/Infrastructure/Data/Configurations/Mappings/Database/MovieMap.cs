﻿using empresas_dotnet.Domain.AggregateModels;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace empresas_dotnet.Infrastructure.Data.Configurations.Mappings.Database
{
    public class MovieMap : IEntityTypeConfiguration<Movie>
    {
        public void Configure(EntityTypeBuilder<Movie> builder)
        {
            builder.ToTable("Movies");
            builder.HasKey(movie => movie.Id);

            builder.Property(movie => movie.Title)
                .UsePropertyAccessMode(PropertyAccessMode.Field)
                .HasColumnName("title")
                .IsRequired();
            
            builder.Property(movie => movie.ReleaseYear)
                .UsePropertyAccessMode(PropertyAccessMode.Field)
                .HasColumnName("releaseYear")
                .IsRequired();
            
            builder.Property(movie => movie.Genre)
                .UsePropertyAccessMode(PropertyAccessMode.Field)
                .HasColumnName("genre")
                .IsRequired();
            
            builder.Property(movie => movie.Actors)
                .UsePropertyAccessMode(PropertyAccessMode.Field)
                .HasColumnName("actors")
                .IsRequired();
            
            builder.Property(movie => movie.AverageGrade)
                .UsePropertyAccessMode(PropertyAccessMode.Field)
                .HasColumnName("averageGrade")
                .IsRequired();


            builder.OwnsMany<Grade>(movie => movie.Grades);
        }
    }
}