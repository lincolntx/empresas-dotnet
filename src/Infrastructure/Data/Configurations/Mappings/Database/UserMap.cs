﻿using empresas_dotnet.Domain.AggregateModels;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace empresas_dotnet.Infrastructure.Data.Configurations.Mappings.Database
{
    public class UserMap : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.ToTable("User");

            builder.HasKey(user => user.Id);
            builder.HasIndex("_email").IsUnique();
            
            
            builder.Property<string>(user => user.Name)
                .HasField("_name")
                .UsePropertyAccessMode(PropertyAccessMode.Field)
                .HasColumnName("name")
                .IsRequired();
            
            builder.Property<string>(user => user.Surname)
                .HasField("_surname")
                .UsePropertyAccessMode(PropertyAccessMode.Field)
                .HasColumnName("surname")
                .IsRequired();
            
            builder.Property<string>(user => user.Email)
                .HasField("_email")
                .UsePropertyAccessMode(PropertyAccessMode.Field)
                .HasColumnName("email")
                .IsRequired();

            builder.Property<string>("_password")
                .UsePropertyAccessMode(PropertyAccessMode.Field)
                .HasColumnName("password")
                .IsRequired();
            
            builder.Property<byte[]>("_passwordSalt")
                .UsePropertyAccessMode(PropertyAccessMode.Field)
                .HasColumnName("passwordSalt")
                .IsRequired();
            
            builder.Property<bool>(user => user.IsAdmin)
                .HasField("_isAdmin")
                .UsePropertyAccessMode(PropertyAccessMode.Field)
                .HasColumnName("isAdmin")
                .IsRequired();
        }
    }
}