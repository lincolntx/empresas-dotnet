﻿using System.Threading.Tasks;
using empresas_dotnet.Data;
using empresas_dotnet.Domain.SeedWork;

namespace empresas_dotnet.Infrastructure.Data.Configurations
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly ApplicationDbContext _applicationDbContext;

        public UnitOfWork(ApplicationDbContext applicationDbContext)
        {
            _applicationDbContext = applicationDbContext;
        }

        public async Task<bool> Commit()
        {
            return await _applicationDbContext.SaveEntitiesAsync();
        }

        public void Dispose()
        {
            _applicationDbContext.Dispose();
        }
    }
}