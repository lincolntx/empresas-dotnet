﻿using System.Collections.Generic;
using System.Threading.Tasks;
using empresas_dotnet.Domain.AggregateModels;
using empresas_dotnet.Domain.SeedWork;

namespace empresas_dotnet.Infrastructure.Data.Repositories.MovieRepository
{
    public interface IMovieRepository : IRepository<Movie>
    {
        Task<List<Movie>> ListMovies();
        Task<Movie> GetMovieById(string id);
        Task<Movie> AddRateToMovie(string id, Grade grade);
    }
}