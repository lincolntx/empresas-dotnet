﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using empresas_dotnet.Data;
using empresas_dotnet.Domain.AggregateModels;
using Microsoft.EntityFrameworkCore;

namespace empresas_dotnet.Infrastructure.Data.Repositories.MovieRepository
{
    public class MovieRepository : Repository<Movie>, IMovieRepository
    {
        public MovieRepository(ApplicationDbContext applicationDbContext) : base(applicationDbContext)
        {
        }

        public async Task<List<Movie>> ListMovies()
        {
            var movies =  await _dbSet.AsNoTracking().ToListAsync();
            return movies.OrderBy(movie => movie.Title).ToList();
        }

        public async Task<Movie> GetMovieById(string id)
        {
            return await _dbSet.AsNoTracking().FirstOrDefaultAsync(movie => movie.Id.ToString() == id);
        }

        public async Task<Movie> AddRateToMovie(string id, Grade grade)
        {
            var found = await _dbSet.FirstOrDefaultAsync(old => old.Id.ToString() == id);
            found.AddVGradeToMovie(grade);

            return found;
        }
    }
}