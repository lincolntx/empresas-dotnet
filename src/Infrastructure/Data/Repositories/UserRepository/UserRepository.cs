﻿using System.Threading.Tasks;
using empresas_dotnet.Data;
using empresas_dotnet.Domain.AggregateModels;
using Microsoft.EntityFrameworkCore;

namespace empresas_dotnet.Infrastructure.Data.Repositories.UserRepository
{
    public class UserRepository : Repository<User>, IUserRepository
    {
        public UserRepository(ApplicationDbContext applicationDbContext) : base(applicationDbContext)
        {
        }

        public async Task<User> FindUserByEmail(string email)
        {
            return await _dbSet.AsNoTracking().FirstOrDefaultAsync(user => user.Email == email);
        }
    }
}