﻿using System.Threading.Tasks;
using empresas_dotnet.Domain.AggregateModels;
using empresas_dotnet.Domain.SeedWork;

namespace empresas_dotnet.Infrastructure.Data.Repositories.UserRepository
{
    public interface IUserRepository : IRepository<User>
    {
        Task<User> FindUserByEmail(string email);
    }
}