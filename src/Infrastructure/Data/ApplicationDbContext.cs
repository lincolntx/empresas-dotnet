﻿using System;
using System.Threading;
using System.Threading.Tasks;
using empresas_dotnet.Infrastructure.Data.Configurations;
using empresas_dotnet.Infrastructure.Data.Configurations.Mappings.Database;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;

namespace empresas_dotnet.Data
{
public class ApplicationDbContext : DbContext
	{
		private IDbContextTransaction _currentTransaction;
		public IDbContextTransaction GetCurrentTransaction() => _currentTransaction;
		public bool HasActiveTransaction => _currentTransaction != null;
		private readonly IMediator _mediator;

		public ApplicationDbContext()
		{
		}

		public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
		{
		}

		public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options, IMediator mediator) : base(options)
		{
			_mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
		}

		protected override void OnConfiguring(DbContextOptionsBuilder options) =>
			options.UseNpgsql(Environment.GetEnvironmentVariable("ConnectionString"),
				npgsqlOptionsAction: pgOptions =>
				{
					pgOptions.EnableRetryOnFailure(maxRetryCount: 3, maxRetryDelay: TimeSpan.FromSeconds(30), errorCodesToAdd: null);
				}
		);

		protected override void OnModelCreating(ModelBuilder modelBuilder)
		{
			modelBuilder.ApplyConfiguration(new UserMap());
			modelBuilder.ApplyConfiguration(new MovieMap());
		}

		public async Task<bool> SaveEntitiesAsync(CancellationToken cancellationToken = default(CancellationToken))
		{
			await _mediator.DispatchDomainEventsAsync(this);

			return await base.SaveChangesAsync(cancellationToken) > 0;
		}
	}
}