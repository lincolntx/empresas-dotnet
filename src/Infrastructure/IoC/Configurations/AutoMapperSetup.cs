﻿using System;
using AutoMapper;
using empresas_dotnet.Application.MappingProfiles;
using Microsoft.Extensions.DependencyInjection;

namespace empresas_dotnet.Infrastructure.IoC.Configurations
{
    public static class AutoMapperSetup
    {
        public static void AddAutoMapper(this IServiceCollection services)
        {
            if (services == null) throw new ArgumentNullException(nameof(services));

            services.AddAutoMapper(typeof(MappingProfile));
        }
    }
}