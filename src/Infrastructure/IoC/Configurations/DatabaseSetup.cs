﻿using System;
using empresas_dotnet.Data;
using Microsoft.Extensions.DependencyInjection;

namespace empresas_dotnet.Infrastructure.IoC.Configurations
{
    public static class DatabaseSetup
    {
        public static void AddDatabaseSetup(this IServiceCollection services)
        {
            if (services == null) throw new ArgumentNullException(nameof(services));

            services.AddDbContext<ApplicationDbContext>(ServiceLifetime.Scoped);
        }
    }
}