﻿using empresas_dotnet.Application.Helpers.Identity;
using MediatR;
using empresas_dotnet.Domain.Exceptions;
using empresas_dotnet.Domain.SeedWork;
using empresas_dotnet.Infrastructure.Data.Configurations;
using empresas_dotnet.Infrastructure.Data.Repositories.MovieRepository;
using empresas_dotnet.Infrastructure.Data.Repositories.UserRepository;
using empresas_dotnet.Infrastructure.Environment;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace empresas_dotnet.Infrastructure.IoC
{
    public class NativeInjector
    {
        public static void RegisterServices(IServiceCollection services, IConfiguration configuration)
        {
            RegisterData(services);
            RegisterEnvironment(services, configuration);
            RegisterHelpers(services);
        }

        public static void RegisterData(IServiceCollection services)
        {
            services.AddScoped<IUnitOfWork, UnitOfWork>();
            services.AddScoped<IUserRepository, UserRepository>();
            services.AddScoped<IMovieRepository, MovieRepository>();
        }
        
        
        public static void RegisterHelpers(IServiceCollection services)
        {
            services.AddScoped<IIdentityService, IdentityService>();
        }
        
        public static void RegisterEnvironment(IServiceCollection services, IConfiguration configuration)
        {
            services.AddSingleton(configuration.GetSection("JwtSettings").Get<JwtSettings>());
        }
    }
}