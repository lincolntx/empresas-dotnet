﻿using System;
using empresas_dotnet.Domain.SeedWork;

namespace empresas_dotnet.Domain.AggregateModels
{
    public class Grade : ValueObject
    {
        public Guid UserId { get; private set; }
        public int GradeValue { get; private set; }

        public Grade(Guid userId, int gradeValue)
        {
            UserId = userId;
            GradeValue = gradeValue;
        }
    }
}