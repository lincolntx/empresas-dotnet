﻿using System;
using System.Security.Cryptography;
using empresas_dotnet.Domain.SeedWork;
using Microsoft.AspNetCore.Cryptography.KeyDerivation;

namespace empresas_dotnet.Domain.AggregateModels
{
    public class User : Entity, IAggregateRoot
    {
        private string _name;
        public string Name => _name;
        private string _surname;
        public string Surname => _surname;
        private string _email;
        public string Email => _email;
        private string _password;
        private byte[] _passwordSalt;
        private bool _isAdmin;
        public bool IsAdmin => _isAdmin;

        protected User() { }
        public User(string name, string surname, string email, string password, bool isAdmin)
        {
            _name = name;
            _surname = surname;
            _email = email;
            _passwordSalt = GenerateSalt();
            _password = CreateHashPassword(password, _passwordSalt);
            _isAdmin = isAdmin;
        }
        
        public bool VerifyUserPassword(string password)
        {
            var requestPasswordSalted = CreateHashPassword(password, _passwordSalt);

            return String.Equals(requestPasswordSalted, _password);
        }
        
        private  string CreateHashPassword(string password, byte [] salt)
        {
            return Convert.ToBase64String(KeyDerivation.Pbkdf2(
                password: password,
                salt: salt,
                prf: KeyDerivationPrf.HMACSHA1,
                iterationCount: 10000,
                numBytesRequested: 256 /8));
        }

        private byte[] GenerateSalt()
        {
            var salt = new byte[128 / 8];
            using var rng = RandomNumberGenerator.Create();
            rng.GetBytes(salt);

            return salt;
        }
    }
}