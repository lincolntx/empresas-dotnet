﻿using System;
using System.Collections.Generic;
using System.Linq;
using empresas_dotnet.Domain.SeedWork;

namespace empresas_dotnet.Domain.AggregateModels
{
    public class Movie : Entity, IAggregateRoot
    {
        public  string Title {get; private set;}
        public  string ReleaseYear {get; private set;}
        public  int Genre {get; private set;}
        public  List<string> Actors {get; private set;}
        public  List<Grade> Grades {get; private set;}
        public  double AverageGrade {get; private set;}

        public Movie(string title, string releaseYear, int genre, List<string> actors)
        {
            Title = title;
            ReleaseYear = releaseYear;
            Genre = genre;
            Actors = actors;
            Grades = new List<Grade>();
            AverageGrade = CalculateAverageGrade();
        }

        public double CalculateAverageGrade()
        {
            List<int> allGradesForMovie = new List<int>();
            if (Grades.Count <= 0)
                return 0.0;
            
            Grades.ForEach(grade => allGradesForMovie.Add(grade.GradeValue));
            return Math.Round(allGradesForMovie.Average(), 2);
        }

        public void AddVGradeToMovie(Grade grade)
        {
            Grades.Add(grade);
            AverageGrade = CalculateAverageGrade();
        }
    }
}