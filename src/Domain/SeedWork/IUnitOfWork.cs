﻿using System;
using System.Threading.Tasks;

namespace empresas_dotnet.Domain.SeedWork
{
    public interface IUnitOfWork : IDisposable
    {
        Task<bool> Commit();
    }
}