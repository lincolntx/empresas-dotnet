﻿using MediatR;

namespace empresas_dotnet.Domain.Exceptions
{
    public class ExceptionNotification : INotification
    {
        public string Code { get; private set; }
        public string Message { get; private set; }
        public string ParamName { get; private set; }

        public ExceptionNotification(string code, string message, string paramName = null)
        {
            Code = code;
            Message = message;
            ParamName = paramName;
        }
    }
}