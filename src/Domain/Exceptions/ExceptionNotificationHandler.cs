﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;

namespace empresas_dotnet.Domain.Exceptions
{
    public class ExceptionNotificationHandler : INotificationHandler<ExceptionNotification>
    {
        private static List<ExceptionNotification> _notifications = new List<ExceptionNotification>();
		
        public ExceptionNotificationHandler()
        {
            
        }
		
        public Task Handle(ExceptionNotification message, CancellationToken cancellationToken)
        {
            _notifications.Add(message);
			
            return Task.CompletedTask;
        }

        public virtual List<ExceptionNotification> GetNotifications()
        {
            return _notifications;
        }

        public virtual bool HasNotifications()
        {
             var lixo = GetNotifications().Any();
             return lixo;
        }

        public void Dispose()
        {
            _notifications = new List<ExceptionNotification>();
        }
    }
}