﻿using empresas_dotnet.Application.Contracts.v1.Requests;
using FluentValidation;

namespace empresas_dotnet.Application.Validations
{
    public class InsertMovieValidations : AbstractValidator<InsertMovieRequest>
    {
        public InsertMovieValidations()
        {
            ValidateTitle();
            ValidateReleaseYear();
            ValidateGenre();
            ValidateActors();

        }
        
        protected void ValidateTitle()
        {
            RuleFor(movie => movie.Title)
                .NotEmpty().WithMessage("Campo Title obrigatório").WithErrorCode("001");
        }

        protected void ValidateReleaseYear()
        {
            RuleFor(movie => movie.ReleaseYear)
                .NotEmpty().WithMessage("Campo ReleaseYear é obrigatório").WithErrorCode("002");
        }
        
        protected void ValidateGenre()
        {
            RuleFor(movie => movie.Genre)
                .NotEmpty().WithMessage("Campo Genre é obrigatório").WithErrorCode("003");
        }
        
        protected void ValidateActors()
        {
            RuleFor(movie => movie.Actors)
                .NotEmpty().WithMessage("Campo Actors é obrigatório").WithErrorCode("004");
        }
    }
}