﻿using empresas_dotnet.Application.Contracts.v1.Requests;
using FluentValidation;

namespace empresas_dotnet.Application.Validations
{
    public class LoginValidations : AbstractValidator<LoginUserRequest>
    {
        public LoginValidations()
        {
            ValidateEmail();
            ValidatePassword();
        }
        
        protected void ValidateEmail()
        {
            RuleFor(login => login.Email)
                .NotEmpty().WithMessage("Campo email obrigatório").WithErrorCode("001")
                .EmailAddress().WithMessage("O campo precisa ser um email válido").WithErrorCode("002");
        }

        protected void ValidatePassword()
        {
            RuleFor(login => login.Password)
                .NotEmpty().WithMessage("Campo senha é obrigatório").WithErrorCode("003")
                .MinimumLength(6).WithMessage("A senha deve ter no mínimo 6 caratcetres").WithErrorCode("004");
        }
    }
}