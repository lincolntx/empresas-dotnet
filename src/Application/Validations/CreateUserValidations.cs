﻿using System;
using empresas_dotnet.Application.Contracts.v1.Requests;
using FluentValidation;

namespace empresas_dotnet.Application.Validations
{
    public class CreateUserValidations : AbstractValidator<CreateUserRequest>
    {
        public CreateUserValidations()
        {
            ValidateName();
            ValidateSurname();
            ValidateEmail();
            ValidatePassword();
        }
        
        protected void ValidateName()
        {
            RuleFor(createUser => createUser.Name)
                .NotEmpty().WithMessage("Campo nome obrigatório").WithErrorCode("001");
        }
        
        protected void ValidateSurname()
        {
            RuleFor(createUser => createUser.Surname)
                .NotEmpty().WithMessage("Campo sobrenome obrigatório").WithErrorCode("002");
        }
        
        protected void ValidateEmail()
        {
            RuleFor(createUser => createUser.Email)
                .NotEmpty().WithMessage("Campo email obrigatório").WithErrorCode("003")
                .EmailAddress().WithMessage("O campo precisa ser um email válido").WithErrorCode("004");
        }
        
        protected void ValidatePassword()
        {
            RuleFor(createUser => createUser.Password)
                .NotEmpty().WithMessage("Campo senha obrigatório").WithErrorCode("005")
                .MinimumLength(6).WithMessage("A senha deve ter no mínimo 6 caratcetres").WithErrorCode("006");
            
            RuleFor(createUser => createUser).Custom((createUser, context) =>
            {
                if (!String.Equals(createUser.Password, createUser.PasswordConfirmation))
                    context.AddFailure(nameof(createUser.Password), "As senhas devem ser iguais");
            });
        }
    }
}