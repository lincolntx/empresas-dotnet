﻿using empresas_dotnet.Application.Contracts.v1.Requests;
using FluentValidation;

namespace empresas_dotnet.Application.Validations
{
    public class RateMovieValidations : AbstractValidator<RateMovieRequest>
    {
        public RateMovieValidations()
        {
            ValidateTitle();
            ValidateRateValue();
        }
        
        protected void ValidateTitle()
        {
            RuleFor(rate => rate.MovieId)
                .NotEmpty().WithMessage("Campo Id é obrigatório").WithErrorCode("001");
        }
        protected void ValidateRateValue()
        {
            RuleFor(rate => rate.RateValue)
                .NotEmpty().WithMessage("Campo RateValue é obrigatório").WithErrorCode("002")
                .LessThan(5).WithMessage("A maior nota permitida é 4").WithErrorCode("003")
                .GreaterThan(0). WithMessage("A menor nota permitida é 0").WithErrorCode("004");
        }
        
    }
}