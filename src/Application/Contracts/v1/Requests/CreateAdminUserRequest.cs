﻿using empresas_dotnet.Application.Contracts.v1.Requests.Base;
using empresas_dotnet.Application.Contracts.v1.Responses;
using empresas_dotnet.Application.Validations;
using MediatR;

namespace empresas_dotnet.Application.Contracts.v1.Requests
{
    public class CreateAdminUserRequest : Request, IRequest<CreateUserResponse>
    {
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string PasswordConfirmation { get; set; }
        
        public override bool IsValid()
        {
            ValidationResult = new CreateAdminValidations().Validate(this);

            return ValidationResult.IsValid;
        }
    }
}