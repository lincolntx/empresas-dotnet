﻿using empresas_dotnet.Application.Contracts.v1.Requests.Base;
using empresas_dotnet.Application.Contracts.v1.Responses;
using empresas_dotnet.Application.Validations;
using MediatR;

namespace empresas_dotnet.Application.Contracts.v1.Requests
{
    public class RateMovieRequest : Request, IRequest<RateMovieResponse>
    {
        public string MovieId { get; set; }
        public int RateValue { get; set; }
        public string? UserId { get; set; }
        public override bool IsValid()
        {
            ValidationResult = new RateMovieValidations().Validate(this);
            return ValidationResult.IsValid;
        }
    }
}