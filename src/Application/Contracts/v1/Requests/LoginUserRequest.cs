﻿using empresas_dotnet.Application.Contracts.v1.Requests.Base;
using empresas_dotnet.Application.Contracts.v1.Responses;
using empresas_dotnet.Application.Validations;
using MediatR;

namespace empresas_dotnet.Application.Contracts.v1.Requests
{
    public class LoginUserRequest : Request, IRequest<LoginResponse>
    {
        public string Email { get; set; }
        public string Password { get; set; }
        
        public override bool IsValid()
        {
            ValidationResult = new LoginValidations().Validate(this);

            return ValidationResult.IsValid;
        }
    }
}