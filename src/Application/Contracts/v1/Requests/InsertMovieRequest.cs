﻿using System.Collections.Generic;
using empresas_dotnet.Application.Contracts.v1.Requests.Base;
using empresas_dotnet.Application.Contracts.v1.Responses;
using empresas_dotnet.Application.Validations;
using MediatR;

namespace empresas_dotnet.Application.Contracts.v1.Requests
{
    public class InsertMovieRequest : Request, IRequest<InsertMovieResponse>
    {
        public string Title { get; set; }
        public string ReleaseYear { get; set; }
        public int Genre { get; set; }
        public List<string> Actors { get; set; }
        
        public override bool IsValid()
        {
            ValidationResult = new InsertMovieValidations().Validate(this);
            return ValidationResult.IsValid;
        }
    }
}