﻿
using FluentValidation.Results;
using MediatR;

namespace empresas_dotnet.Application.Contracts.v1.Requests.Base
{
    public abstract class Request
    { 
        protected ValidationResult ValidationResult { get; set; }

        public ValidationResult GetValidationResult()
        {
            return ValidationResult;
        }

        public abstract bool IsValid();
    }
}