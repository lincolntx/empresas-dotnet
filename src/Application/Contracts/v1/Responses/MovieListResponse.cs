﻿using System.Collections.Generic;

namespace empresas_dotnet.Application.Contracts.v1.Responses
{
    public class MovieListResponse
    {
        public string Id { get; set; }
        public string Title { get; set; }
        public int Gender { get; set;}
        public List<string> Actor { get; set; }
        public double AverageGrade { get; set; }

        public MovieListResponse(string id, string title, int gender, List<string> actor, double averageGrade)
        {
            Id = id;
            Title = title;
            Gender = gender;
            Actor = actor;
            AverageGrade = averageGrade;
        }
    }
}