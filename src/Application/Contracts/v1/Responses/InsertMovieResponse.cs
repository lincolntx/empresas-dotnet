﻿using System.Collections.Generic;

namespace empresas_dotnet.Application.Contracts.v1.Responses
{
    public class InsertMovieResponse
    {
        public string Title { get; set; }
        public string ReleaseYear { get; set; }
        public string Genre { get; set; }
        public List<string> Actors { get; set; }
        public double AverageGrade { get; set; }

        public InsertMovieResponse(string title, string releaseYear, string genre, List<string> actors, double averageGrade)
        {
            Title = title;
            ReleaseYear = releaseYear;
            Genre = genre;
            Actors = actors;
            AverageGrade = averageGrade;
        }
    }
}