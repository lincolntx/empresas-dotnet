﻿namespace empresas_dotnet.Application.Contracts.v1.Responses
{
    public class LoginResponse
    {
        public string Email { get; set; }
        public string Name { get; set; }
        public string Token { get; set; }
        public string RefreshToken { get; set; }

        public LoginResponse(string email, string name, string token, string refreshToken)
        {
            Email = email;
            Name = name;
            Token = token;
            RefreshToken = refreshToken;
        }
    }
}