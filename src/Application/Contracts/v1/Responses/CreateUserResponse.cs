﻿namespace empresas_dotnet.Application.Contracts.v1.Responses
{
    public class CreateUserResponse
    {
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Email { get; set; }
        public string Message { get; set; }

        public CreateUserResponse(string name, string surname, string email)
        {
            Name = name;
            Surname = surname;
            Email = email;
            Message = "Usuário cadastrado com sucesso!";
        }
    }
}