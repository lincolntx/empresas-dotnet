﻿namespace empresas_dotnet.Application.Contracts.v1.Responses
{
    public class RateMovieResponse
    {
        public string Movie { get; set; }
        public int Rate { get; set; }

        public RateMovieResponse(string movie, int rate)
        {
            Movie = movie;
            Rate = rate;
        }
    }
}