﻿using System.Threading;
using System.Threading.Tasks;
using empresas_dotnet.Application.Contracts.v1.Requests;
using empresas_dotnet.Application.Contracts.v1.Responses;
using empresas_dotnet.Application.RequestHandlers.Base;
using empresas_dotnet.Domain.AggregateModels;
using empresas_dotnet.Domain.Exceptions;
using empresas_dotnet.Domain.SeedWork;
using empresas_dotnet.Infrastructure.Data.Repositories.MovieRepository;
using MediatR;

namespace empresas_dotnet.Application.RequestHandlers
{
    public class InsertMovieRequestHandler : RequestHandler, IRequestHandler<InsertMovieRequest,InsertMovieResponse>
    {
        private readonly IMovieRepository _movieRepository;
        public InsertMovieRequestHandler(IUnitOfWork uow, IMediator bus, INotificationHandler<ExceptionNotification> notifications,
            IMovieRepository movieRepository) : base(uow, bus, notifications)
        {
            _movieRepository = movieRepository;
        }

        public async Task<InsertMovieResponse> Handle(InsertMovieRequest request, CancellationToken cancellationToken)
        {
            if (!request.IsValid())
            {
                NotifyValidationErrors(request);
                return null;
            }
            
            var movie = new Movie(request.Title, request.ReleaseYear, request.Genre, request.Actors);
            _movieRepository.Add(movie);

            if (await Commit())
            {
                return new InsertMovieResponse(movie.Title, movie.ReleaseYear, movie.Genre.ToString(), movie.Actors,
                    movie.AverageGrade);
            }

            return null;
        }
    }
}