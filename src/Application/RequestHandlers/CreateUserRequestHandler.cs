﻿using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using empresas_dotnet.Application.Contracts.v1.Requests;
using empresas_dotnet.Application.Contracts.v1.Responses;
using empresas_dotnet.Application.RequestHandlers.Base;
using empresas_dotnet.Domain.AggregateModels;
using empresas_dotnet.Domain.Exceptions;
using empresas_dotnet.Domain.SeedWork;
using empresas_dotnet.Infrastructure.Data.Repositories.UserRepository;
using MediatR;

namespace empresas_dotnet.Application.RequestHandlers
{
    public class CreateUserRequestHandler : RequestHandler, IRequestHandler<CreateUserRequest, CreateUserResponse>
    {
        private readonly IUserRepository _userRepository;
        private readonly IMapper _mapper;
        
        public CreateUserRequestHandler(IUnitOfWork uow, IMediator bus, INotificationHandler<ExceptionNotification> notifications,
            IUserRepository userRepository, IMapper mapper) : base(uow, bus, notifications)
        {
            _userRepository = userRepository;
            _mapper = mapper;
        }

        public async Task<CreateUserResponse> Handle(CreateUserRequest request, CancellationToken cancellationToken)
        {
            if (!request.IsValid())
            {
                NotifyValidationErrors(request);
                return null;
            }
            
            var user = new User(request.Name, request.Surname, request.Email, request.Password, false);
            var createdUSer = _mapper.Map<CreateUserResponse>(user);
            
            _userRepository.Add(user);

            if (await Commit())
            {
                return createdUSer;
            }

            return null;
        }
    }
}