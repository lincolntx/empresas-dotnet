﻿using System.Threading;
using System.Threading.Tasks;
using empresas_dotnet.Application.Contracts.v1.Requests;
using empresas_dotnet.Application.Contracts.v1.Responses;
using empresas_dotnet.Application.Helpers.Identity;
using empresas_dotnet.Application.RequestHandlers.Base;
using empresas_dotnet.Domain.Exceptions;
using empresas_dotnet.Domain.SeedWork;
using empresas_dotnet.Infrastructure.Data.Repositories.UserRepository;
using MediatR;

namespace empresas_dotnet.Application.RequestHandlers
{
    public class LoginUserRequestHandler : RequestHandler, IRequestHandler<LoginUserRequest,LoginResponse>
    {
        private readonly IUserRepository _userRepository;
        private readonly IIdentityService _identityService;
        
        public LoginUserRequestHandler(IUnitOfWork uow, IMediator bus, INotificationHandler<ExceptionNotification> notifications,
            IUserRepository userRepository, IIdentityService identityService) : base(uow, bus, notifications)
        {
            _userRepository = userRepository;
            _identityService = identityService;
        }

        public async Task<LoginResponse> Handle(LoginUserRequest request, CancellationToken cancellationToken)
        {
            if (!request.IsValid())
            {
                NotifyValidationErrors(request);
                return null;
            }

            var user = await _userRepository.FindUserByEmail(request.Email);
            var isPasswordValid = user.VerifyUserPassword(request.Password);

            if (!isPasswordValid)
            {
                await _bus.Publish(new ExceptionNotification("001", "Usuário ou senha inválidos"));
                return null;
            }

            var token = _identityService.GenerateAuthenticationResultForUserAsync(user);
            
            return new LoginResponse(user.Email, user.Name, token.Token, token.RefreshToken);
        }
    }
}