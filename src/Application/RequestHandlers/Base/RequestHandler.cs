﻿using System.Threading.Tasks;
using empresas_dotnet.Application.Contracts.v1.Requests.Base;
using empresas_dotnet.Domain.Exceptions;
using empresas_dotnet.Domain.SeedWork;
using MediatR;

namespace empresas_dotnet.Application.RequestHandlers.Base
{
    public abstract class RequestHandler
    {
        private readonly IUnitOfWork _uow;
        protected readonly IMediator _bus;
        private readonly ExceptionNotificationHandler _notifications;

        protected RequestHandler(IUnitOfWork uow, IMediator bus, INotificationHandler<ExceptionNotification> notifications)
        {
            _uow = uow;
            _bus = bus;
            _notifications = (ExceptionNotificationHandler)notifications;
        }

        protected void NotifyValidationErrors(Request message)
        {
            foreach (var error in message.GetValidationResult().Errors)
            {
                _bus.Publish(new ExceptionNotification("001", error.ErrorMessage, error.PropertyName));
            }
        }

        public async Task<bool> Commit()
        {
            if (_notifications.HasNotifications()) return false;
            if (await _uow.Commit()) return true;

            await _bus.Publish(new ExceptionNotification("002", "We had a problem during saving your data."));

            return false;
        }
    }
}