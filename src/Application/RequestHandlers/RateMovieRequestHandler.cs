﻿using System;
using System.Threading;
using System.Threading.Tasks;
using empresas_dotnet.Application.Contracts.v1.Requests;
using empresas_dotnet.Application.Contracts.v1.Responses;
using empresas_dotnet.Application.RequestHandlers.Base;
using empresas_dotnet.Domain.AggregateModels;
using empresas_dotnet.Domain.Exceptions;
using empresas_dotnet.Domain.SeedWork;
using empresas_dotnet.Infrastructure.Data.Repositories.MovieRepository;
using MediatR;

namespace empresas_dotnet.Application.RequestHandlers
{
    public class RateMovieRequestHandler : RequestHandler, IRequestHandler<RateMovieRequest, RateMovieResponse>
    {
        private readonly IMovieRepository _movieRepository;
        
        public RateMovieRequestHandler(IUnitOfWork uow, IMediator bus, INotificationHandler<ExceptionNotification> notifications,
            IMovieRepository movieRepository) : base(uow, bus, notifications)
        {
            _movieRepository = movieRepository;
        }

        public async Task<RateMovieResponse> Handle(RateMovieRequest request, CancellationToken cancellationToken)
        {
            if (!request.IsValid())
            {
                NotifyValidationErrors(request);
                return null;
            }

            
            var movie = await _movieRepository.AddRateToMovie(request.MovieId, new Grade(Guid.Parse(request.UserId), request.RateValue));

            if (await Commit())
            {
                return new RateMovieResponse(movie.Title, request.RateValue);
            }

            return null;
        }
    }
}