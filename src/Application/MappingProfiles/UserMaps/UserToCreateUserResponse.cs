﻿using empresas_dotnet.Application.Contracts.v1.Responses;
using empresas_dotnet.Domain.AggregateModels;

namespace empresas_dotnet.Application.MappingProfiles.UserMaps
{
    public class UserToCreateUserResponse : MappingProfile
    {
        public UserToCreateUserResponse()
        {
            CreateUserResponse();
        }

        public void CreateUserResponse()
        {
            CreateMap<User, CreateUserResponse>()
                .ConstructUsing(user => new CreateUserResponse(user.Name, user.Surname, user.Email));
        }
    }
}