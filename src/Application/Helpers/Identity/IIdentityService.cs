﻿using System.Collections.Generic;
using System.Security.Claims;
using empresas_dotnet.Domain.AggregateModels;

namespace empresas_dotnet.Application.Helpers.Identity
{
    public interface IIdentityService
    {
      AuthenticationResult GenerateAuthenticationResultForUserAsync(User user);
      bool IsTokenValid(string token);
      IEnumerable<Claim> GetTokenClaims(string token);
    }
}